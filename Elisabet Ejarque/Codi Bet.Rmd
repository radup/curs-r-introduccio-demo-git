---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Carregar dades

```{r}
#install.packages("pacman")
library("pacman")
p_load("tidyverse", "haven")

```

```{r}
# Arxiu descarregat d'aquí:
# https://opendata-ajuntament.barcelona.cat/data/ca/dataset/iris
download.file("https://opendata-ajuntament.barcelona.cat/data/dataset/15b349cd-3d4d-4a62-9ad3-d67230029a23/resource/3e40e0ca-75ff-42e2-8fae-c019c0d06f40/download/2019-peticions-ciutadanes.csv", destfile="2019-peticions-ciutadanes.csv")
df <- read.csv("2019-peticions-ciutadanes.csv", 
         encoding="UTF-8")
```


```{r}

df1 <- df%>% select(FITXA_ID, DISTRICTE, TIPUS) %>%
  group_by(DISTRICTE) %>%
  summarise(Total_incidencies = n())

df1
```







